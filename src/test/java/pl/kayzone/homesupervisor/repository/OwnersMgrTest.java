package pl.kayzone.homesupervisor.repository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import pl.kayzone.homesupervisor.entity.Owners;

@RunWith(SpringRunner.class)
@SpringBootTest
public class OwnersMgrTest
{
   @Autowired
   private OwnersMgr ownersMgr;

   @Test
   public void firstSaveTest() {
      Owners own = new Owners();
      own.setName("Skrobiszewscy");
      own.setOwner_type("OWNER");

      ownersMgr.save(own);

   }
}
