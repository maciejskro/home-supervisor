package pl.kayzone.homesupervisor.repository;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import pl.kayzone.homesupervisor.entity.Buildings;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BuildingsMgrTest{

   @Autowired
   private BuildingsMgr buildingsMgr;

   @Test
   public void firstSaveTest() {
      Buildings build = new Buildings();
            build.setName("CB10");
            build.setNumber_locals(144);
            build.setBuild_number("10");
            build.setStreet("osiedle Centrum B");

            buildingsMgr.save(build);

      Assertions.assertThat(buildingsMgr.findById(1L));
   }
}
