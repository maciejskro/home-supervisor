
create database if not exists home_supervisor;


create schema if not exists inventory;
create schema if not exists persons;

create schema if not exists readings;
create schema if not exists documents;

create sequence inventory.building_id_seq if not exists increment 1;
create sequence inventory.flats_id_seq if not exists  increment 1;
create sequence inventory.measure_items_id_seq if not exists increment 1;

create sequence persons.owners_id_seq if not exists increment 1;
create sequence persons.person_id_seq if not exists increment 1;

create sequence "public".users_id_seq if not exists increment 1;
create sequence readings.reading_meters_id_seq if not exists increment 1;