package pl.kayzone.homesupervisor.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig
{

/*   @Autowired
   private UserSecurityService userSecurityService;*/

   protected void configure(HttpSecurity http) throws Exception {
      http.authorizeRequests()
            .antMatchers("/webjars/**").permitAll()
            .antMatchers("/css/**").permitAll()
            .antMatchers("/js/**").authenticated()
            .antMatchers("/fragments/**").authenticated()
            .antMatchers("/layouts/**").authenticated()
            .antMatchers("/v1/trans/**").permitAll()
           // .antMatchers("/v1/currency/**").permitAll()
           // .antMatchers("/v1/courses/**").permitAll()
            .antMatchers("/").authenticated()
            .antMatchers("/user/**").hasAnyRole("ADMIN")
           // .antMatchers("/currency/**").hasAnyRole("ADMIN")
           // .antMatchers("/transaction").authenticated()
            .antMatchers("/books/all").hasAnyRole("USER","ADMIN")
            .anyRequest()
            .authenticated()
            .and()
            .formLogin()
            .loginPage("/login").permitAll()
            .and()
            .logout().logoutSuccessUrl("/login").permitAll()
            .disable();
      //http.csrf().disable();
   }

}
