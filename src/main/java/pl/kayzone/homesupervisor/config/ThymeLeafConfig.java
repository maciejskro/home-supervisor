package pl.kayzone.homesupervisor.config;

import java.util.Arrays;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.thymeleaf.spring5.ISpringTemplateEngine;
import org.thymeleaf.spring5.SpringTemplateEngine;
import org.thymeleaf.spring5.templateresolver.SpringResourceTemplateResolver;
import org.thymeleaf.spring5.view.ThymeleafViewResolver;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.ITemplateResolver;

@Configuration
@EnableWebMvc
public class ThymeLeafConfig implements ApplicationContextAware
{
   private ApplicationContext applicationContext;

   private static final String UTF8 = "UTF-8";

   @Override public void setApplicationContext(ApplicationContext applicationContext) throws BeansException
   {
      this.applicationContext = applicationContext;
   }

   @Bean
   public ThymeleafViewResolver htmlViewResolver() {
      ThymeleafViewResolver viewResolver = new ThymeleafViewResolver();
      viewResolver.setTemplateEngine(templateEngine(htmlTemplateResolver()));
      viewResolver.setCharacterEncoding("UTF-8");
      viewResolver.setViewNames(new String[]  {".html", ".xhtml", ".js" ,".css"});
      return viewResolver;
   }
   @Bean
   public ViewResolver cssViewResolver() {
      ThymeleafViewResolver resolver = new ThymeleafViewResolver();
      resolver.setTemplateEngine(templateEngine(cssTemplateResolver()));
      resolver.setContentType("text/css");
      resolver.setCharacterEncoding(UTF8);
      resolver.setViewNames ( new String[]{"*.css"});
      return resolver;
   }

   public ISpringTemplateEngine templateEngine(ITemplateResolver templateResolver) {
      SpringTemplateEngine engine = new SpringTemplateEngine();
      engine.setEnableSpringELCompiler(true);
      engine.setTemplateResolver(templateResolver);
      return engine;
   }
   private ITemplateResolver htmlTemplateResolver() {
      SpringResourceTemplateResolver resolver = new SpringResourceTemplateResolver();
      resolver.setApplicationContext(applicationContext);
      resolver.setPrefix("/");
      resolver.setSuffix(".html");
      resolver.setTemplateMode(TemplateMode.HTML);
      return resolver;
   }
   private ITemplateResolver cssTemplateResolver() {
      SpringResourceTemplateResolver resolver = new SpringResourceTemplateResolver();
      resolver.setApplicationContext(applicationContext);
      resolver.setPrefix("/static/css/");
      resolver.setSuffix(".css");
      resolver.setTemplateMode(TemplateMode.CSS);
      return resolver;
   }

   private ITemplateResolver javascriptTemplateResolver() {
      SpringResourceTemplateResolver resolver = new SpringResourceTemplateResolver();
      resolver.setApplicationContext(applicationContext);
      resolver.setPrefix("/static/js/");
      resolver.setSuffix(".js");
      resolver.setTemplateMode(TemplateMode.JAVASCRIPT);
      //esolver.setTemplateMode("HTML");
      return resolver;
   }
   private ITemplateResolver webjarsTemplateResolver() {
      SpringResourceTemplateResolver resolver = new SpringResourceTemplateResolver();
      resolver.setApplicationContext(applicationContext);
      resolver.setPrefix("/webjars");
      resolver.setSuffix(".js");
      resolver.setTemplateMode(TemplateMode.JAVASCRIPT);
      //resolver.setTemplateMode("HTML");
      return resolver;
   }
}
