package pl.kayzone.homesupervisor.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@EnableWebMvc
public class WebConfiguration implements WebMvcConfigurer
{

   @Override
   public void addResourceHandlers(ResourceHandlerRegistry registry) {
      registry.addResourceHandler("/webjars/**")
            .addResourceLocations("/webjars/")
            .resourceChain(false);
      registry.addResourceHandler("/static/css/**" , "/static/js/**" )
            .addResourceLocations("classpath:/static/css/" ,"classpath:/static/js/" )
            .resourceChain(false);

   }

   public void addViewControllers(ViewControllerRegistry registry) {
      //registry.addViewController("/").setViewName("transactions");
      //registry.addViewController("/login").setViewName("login");
      registry.addViewController("/buildingList").setViewName("buildingList");

   }
}
