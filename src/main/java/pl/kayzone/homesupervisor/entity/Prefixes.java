package pl.kayzone.homesupervisor.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(schema = "public", name = "prefix_unit")
public class Prefixes implements Serializable
{
   @Id
   @Column(name = "id")
   private Integer id;
   private String name;
   private String shortName;
   private BigDecimal value;
}
