package pl.kayzone.homesupervisor.entity;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(schema = "readings", name = "reading_meters")
@SequenceGenerator(name = "reading_meters_id_seq", initialValue = 1)
public class ReadingMeters extends BaseEntity
{
   @ManyToOne(cascade = CascadeType.ALL)
   @JoinColumn(name = "measure_item_id")
   private MeasureItems meter;
   private LocalDateTime readingsDateTime;
   private BigDecimal value;
   @ManyToOne
   @JoinColumn(name = "units_id")
   private PhysicalUnit physicalQuantity;
   private Integer basePhysicalUnit;
   private String comment;

   @ManyToOne
   @JoinColumn(name = "users_id")
   private Users usersReadedValue;
}

