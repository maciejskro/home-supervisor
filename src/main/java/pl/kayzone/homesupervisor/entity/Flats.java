package pl.kayzone.homesupervisor.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(schema = "inventory", name = "flats")
@SequenceGenerator(name = "flats_id_seq" , initialValue = 1)
@Getter
@Setter
@NoArgsConstructor
public class Flats extends BaseEntity
{
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name="buildings_id")
   private Buildings buildId;
   @Column(name = "local_number")
   private String localNumber;
   @Column(name = "flat_area")
   private Float flatArea;
   @Column(name = "room_number")
   private Integer roomNumber;
   @Column(name = "level_number")
   private Integer levelNumber;


   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name="owners_id")
   private Owners owners;

   @OneToMany(mappedBy = "flat", cascade = CascadeType.ALL, orphanRemoval = true)
   private List<MeasureItems> measureItems;

}
