package pl.kayzone.homesupervisor.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(schema = "inventory" , name = "Buildings")
@Getter
@Setter
@NoArgsConstructor
@SequenceGenerator(name = "buildings_id_seq", initialValue = 1, allocationSize = 100)
public class Buildings extends BaseEntity
{
   @Column(name = "name")
   private String name;
   @Column(name = "street")
   private String street;
   private String build_number;
   private String zip_code;
   private String city;
   private String region;
   private Integer number_locals;


   @OneToMany(mappedBy = "buildId" , cascade = CascadeType.ALL, orphanRemoval = true)
   private List<Flats> flatsList;
}
