package pl.kayzone.homesupervisor.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(schema = "persons", name = "owners")
@Getter
@Setter
@NoArgsConstructor
@SequenceGenerator(name = "owners_id_seq",initialValue = 1)
public class Owners extends BaseEntity
{

   private String name;
   private String owner_type; //firma lub osoba prywatna
   private String nip;

   @OneToMany(mappedBy = "owners")
   private List<Flats> flats_owner;

   @ManyToMany
   @JoinTable(schema = "persons", name="owners_persons",
         joinColumns = @JoinColumn(name="owner_id") ,inverseJoinColumns = @JoinColumn(name = "person_id"))
   private List<Persons> persons_owner;
}
