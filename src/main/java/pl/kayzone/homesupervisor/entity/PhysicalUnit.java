package pl.kayzone.homesupervisor.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(schema = "public", name="units")
public class PhysicalUnit implements Serializable
{
   @Id
   @GeneratedValue
   @Column(name = "id")
   private Integer id;
   private String name;
   private String shortName;
   private String unitSign;
}
