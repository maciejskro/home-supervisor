package pl.kayzone.homesupervisor.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(schema = "inventory", name = "measure_items")
@SequenceGenerator(name = "measure_items_id_seq", initialValue = 1)
public class MeasureItems extends BaseEntity
{
   private String item_type;
   private String serial_number;

   @ManyToOne
   @JoinColumn(name = "units_id")
   private PhysicalUnit physicalUnit;

   @ManyToOne
   @JoinColumn(name = "flats_id")
   private Flats flat;

   @OneToMany(mappedBy = "meter" , cascade = CascadeType.ALL)
   private List<ReadingMeters> readingMetersList;
}
