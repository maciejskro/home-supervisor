package pl.kayzone.homesupervisor.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(schema = "persons" , name = "person")
@SequenceGenerator(name = "persons_id_seq", initialValue = 1)
public class Persons extends BaseEntity
{
   @Column(name = "fist_name")
   private String first_name;
   @Column(name = "last_name")
   private String last_name;
   @Column(name = "phoneNumber")
   private String phone_number;
   @Column(name = "email")
   private String email;

   @ManyToMany(mappedBy = "persons_owner")
   private List<Owners> iAmOwner;
}
