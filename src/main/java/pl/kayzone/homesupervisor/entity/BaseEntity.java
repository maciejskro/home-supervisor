package pl.kayzone.homesupervisor.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import org.hibernate.annotations.CreationTimestamp;

@MappedSuperclass
abstract  class BaseEntity implements Serializable
{
   @Id
   @Column(name = "id")
   @GeneratedValue(strategy = GenerationType.SEQUENCE)
   private Long id;
   @Column(name = "modification_user_id")
   private Long modification_user_id;
   @Column(name = "modification_date")
   @CreationTimestamp
   private LocalDateTime modification_date;
}
