package pl.kayzone.homesupervisor.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table (schema = "public" ,name = "users")
@SequenceGenerator(name = "users_id_seq",initialValue = 1)
public class Users
{
   @Id
   @Column (name= "id")
   private Long id;
   @Column(name = "name")
   private String fullname;
   @Column(name = "username")
   private String username;
   @Column(name = "pass")
   private String password;
   @Column(name = "phone_number")
   private String phoneNumber;
   @Column(name = "email")
   private String email;
   @Column(name= "role")
   private String role;
}
