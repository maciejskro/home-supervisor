package pl.kayzone.homesupervisor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HomeSupervisorApplication
{

   public static void main(String[] args)
   {
      SpringApplication.run(HomeSupervisorApplication.class, args);
   }

}
