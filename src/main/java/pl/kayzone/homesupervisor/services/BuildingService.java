package pl.kayzone.homesupervisor.services;

import org.springframework.beans.factory.annotation.Autowired;

import pl.kayzone.homesupervisor.entity.Owners;
import pl.kayzone.homesupervisor.repository.OwnersMgr;

public class BuildingService
{

   @Autowired
   private OwnersMgr ownersMgr;

   public Owners  getOwnersByID (Long id ) {
      return ownersMgr.findById(id).get();
   }

}
