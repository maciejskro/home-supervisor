package pl.kayzone.homesupervisor.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import pl.kayzone.homesupervisor.entity.Flats;
import pl.kayzone.homesupervisor.repository.FlatsMgr;

public class FlatServices
{
   @Autowired
   private FlatsMgr flatsMgr;

   public List<Flats> getFlatsByBuild(Long build_id) {
      return flatsMgr.findAllByBuildId(build_id);

   }
}
