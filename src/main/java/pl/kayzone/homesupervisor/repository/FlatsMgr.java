package pl.kayzone.homesupervisor.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import pl.kayzone.homesupervisor.entity.Flats;

@Repository
public interface FlatsMgr extends JpaRepository<Flats, Long>
{
      List<Flats> findAllByBuildId(Long buildId);
}
