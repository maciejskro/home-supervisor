package pl.kayzone.homesupervisor.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import pl.kayzone.homesupervisor.entity.Owners;

@Repository
public interface OwnersMgr extends JpaRepository<Owners,Long>
{

}
