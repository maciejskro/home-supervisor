package pl.kayzone.homesupervisor.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import pl.kayzone.homesupervisor.entity.MeasureItems;

@Repository
public interface MeasureItemsMgr extends JpaRepository<MeasureItems, Long>
{
}
