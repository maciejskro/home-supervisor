package pl.kayzone.homesupervisor.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import pl.kayzone.homesupervisor.entity.Persons;

@Repository
public interface PersonsMgr extends JpaRepository<Persons,Long>
{
}
