package pl.kayzone.homesupervisor.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import pl.kayzone.homesupervisor.entity.Users;

@Repository
public interface UsersMgr extends JpaRepository<Users,Long>
{
}
