package pl.kayzone.homesupervisor.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import pl.kayzone.homesupervisor.entity.PhysicalUnit;

public interface PhysicalUnitMgr extends JpaRepository<PhysicalUnit,Integer>
{
}
