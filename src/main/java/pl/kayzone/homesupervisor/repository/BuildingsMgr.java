package pl.kayzone.homesupervisor.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import pl.kayzone.homesupervisor.entity.Buildings;

@Repository
public interface BuildingsMgr extends JpaRepository<Buildings , Long>
{

}
